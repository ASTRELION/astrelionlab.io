# [astrelion.gitlab.io](https://astrelion.gitlab.io)

Based on the [Gatsby pages template](https://gitlab.com/pages/gatsby)

---

## Running Locally

1. Clone and `cd` into the repository
2. Run `npm install`
3. Run `npm install -g gatsby-cli`
4. Run `gatsby develop` to deploy the site locally ([http://localhost:8000](http://localhost:8000))