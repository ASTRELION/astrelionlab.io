module.exports = {
  pathPrefix: `/`,
  siteMetadata: {
    title: `ASTRELION`,
  },
  plugins: [`gatsby-plugin-react-helmet`],
}
