import React from 'react'
import Link from 'gatsby-link'
import Layout from '../components/layout'

const IndexPage = () => (
    <Layout>
    <div>
        <h1>Hi.</h1>
        <p>I'm Astrelion.</p>
    </div>
    <div 
        style={{
            display: "none",
        }}
    >
        <a rel="me" href="https://mastodon.social/@astrelion">Mastodon</a>
    </div>
    </Layout>
);

export default IndexPage;
