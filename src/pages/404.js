import React from 'react'

const NotFoundPage = () => (
  <div>
    <h1>404 Not Found</h1>
    <p>This page you requested doesn't exist!</p>
  </div>
)

export default NotFoundPage
