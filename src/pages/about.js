import React from 'react'
import Link from 'gatsby-link'

import Layout from '../components/layout'

export default function ContactPage()
{
    return <Layout title="About">
        <div>
            <div>
                <h2>*</h2>
                <div>
                    I like to code things. <br/>
                    It's pronounced as-strel-ee-on if you were curious.
                </div>
            </div>
            <div>
                <br />
                <h2>+</h2>
                <div className="row">
                    <div className="col-sm-2">
                        Website: 
                    </div>
                    <div className="col-sm-10">
                        <a href="https://astrelion.com">astrelion.com</a>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-2">
                        GitLab: 
                    </div>
                    <div className="col-sm-10">
                        <a href="https://gitlab.com/ASTRELION" target="_blank" rel="noreferrer">gitlab.com/ASTRELION</a> | {}
                        <a href="https://dev.astrelion.com" target="_blank" rel="noreferrer">dev.astrelion.com</a>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="col-sm-2">
                        Email:
                    </div>
                    <div className="col-sm-10">
                        <a rel="me" href="mailto:contact@astrelion.com">contact@astrelion.com</a>
                        <sup><a href="../../../contact@astrelion.com-pgp-public.asc">PGP Public Key</a></sup>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-2">
                        Matrix: 
                    </div>
                    <div className="col-sm-10">
                        @astrelion:matrix.org
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-2">
                        Mastodon:
                    </div>
                    <div className="col-sm-10">
                        <a rel="me" href="https://mastodon.social/@astrelion">@astrelion@mastodon.social</a>
                    </div>
                </div>
                <br />
                <div>
                    <a href='https://ko-fi.com/O5O7XRIS' target='_blank' rel="noreferrer">
                        <img style={{border: `0px`, height: `36px`}} src='https://cdn.ko-fi.com/cdn/kofi2.png?v=2' alt='Support me at ko-fi.com' />
                    </a>
                </div>
            </div>
        </div>
        <div>
            <br />
            <h2>-</h2>
            This site was built with <a href="https://gatsbyjs.com" target="_blank" rel="noreferrer">Gatsby</a>.
        </div>
    </Layout>
}
