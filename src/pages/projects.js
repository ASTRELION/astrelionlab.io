import React from 'react'
import Link from 'gatsby-link'

import Layout from '../components/layout'
import Project from '../components/project'
import ProjectStyles from '../components/project.module.css'
import More from '../components/more'

import BadNecromancerImage from '../../static/BadNecromancer_Splash.png'
import LaunchMeImage from '../../static/launchme.gif'
import VanillappImage from '../../static/vpp.png'
import JumperImage from '../../static/jumper.png'
import FocusImage from '../../static/focus.png'
import SimpleDigitalImage from '../../static/simpledigital.png'
import AtlasImage from '../../static/atlas.png'
import Individual437Image from '../../static/individual437.png'
import SpaceSequenceImage from '../../static/spacesequence.png'
import AstrelionImage from '../../static/astrelion.png'

const PROFILE_URL = "https://gitlab.com/ASTRELION/";

const projects = [
    {
        title: "Bad Necromancer",
        description: "Wave-based, rogue-like dungeon crawler",
        languages: "C#",
        platforms: "Made with Unity, dist. for PC",
        category: "Game",
        source: "private",
        image: BadNecromancerImage,
    },
    {
        title: "Keizaal",
        description: "A Skyrim shout plugin for Minecraft",
        languages: "Java",
        platforms: "Minecraft: Java Edition (PC)",
        category: "Minecraft Plugin",
        source: PROFILE_URL + "keizaal",
    },
    {
        title: "LaunchMe",
        description: "A Minecraft plugin for launching yourself atop projectiles you fire",
        link: "https://spigotmc.org/resources/launchme.87512",
        languages: "Java",
        platforms: "Minecraft: Java Edition (PC)",
        category: "Minecraft Plugin",
        source: PROFILE_URL + "launchme",
        image: LaunchMeImage,
    },
    {
        title: "Vanilla++",
        description: "Minecraft plugin that expands on the 'vanilla' experience",
        languages: "Java",
        platforms: "Minecraft: Java Edition (PC)",
        category: "Minecraft Plugin",
        source: PROFILE_URL + "vanillapp",
        image: VanillappImage,
    },
    {
        title: "Jumper",
        description: "Clock face with a simple jumping game",
        link: "https://gallery.fitbit.com/details/239f0247-e2d0-48a9-add9-3ceea49d8960",
        languages: "Javascript, HTML/SVG, CSS",
        platforms: "Fitbit Watches",
        category: "Clock Face",
        source: PROFILE_URL + "jumper",
        image: JumperImage,
    },
    {
        title: "SimpleDigital",
        description: "Simple watch face for Fibit devices",
        link: "https://gallery.fitbit.com/details/a2573b74-3ab8-4d91-9ed3-cfcb9f02810d",
        languages: "Javascript, HTML/SVG, CSS",
        platforms: "Fitbit Watches",
        category: "Clock Face",
        source: PROFILE_URL + "SimpleDigital",
        image: SimpleDigitalImage,
    },
    {
        title: "Focus",
        description: "Ultra-simple watch face for Fibit devices",
        link: "https://gallery.fitbit.com/details/48d57eb9-d675-4178-a95f-e8fed99809ce",
        languages: "Javascript, HTML/SVG, CSS",
        platforms: "Fitbit Watches",
        category: "Clock Face",
        source: PROFILE_URL + "focus",
        image: FocusImage
    },
    {
        title: "Atlas",
        description: "General-purpose Discord bot for servers",
        languages: "Python",
        platforms: "Discord",
        category: "Discord Bot",
        source: PROFILE_URL + "Atlas",
        image: AtlasImage
    },
    {
        title: "ASTRELIOUS",
        description: "Self-bot for Discord (now archived)",
        languages: "Javascript",
        platforms: "Discord",
        category: "Discord Bot",
        source: PROFILE_URL + "ASTRELIOUS",
    },
    {
        title: "G.A.M.E.",
        description: "Raspberry Pi custom game console; Graphics Advanced Machine for Entertainment",
        languages: "C++",
        platforms: "Raspberri Pi",
        category: "University, Raspberry Pi",
        source: PROFILE_URL + "GAME",
    },
    {
        title: "Individual437",
        description: "Individual projects for my game design course",
        languages: "C#",
        platforms: "Monogame and Unity",
        category: "University, Game",
        source: PROFILE_URL + "individual437",
        image: Individual437Image,
    },
    {
        title: "Space Sequence",
        description: "Web-based animation tool",
        languages: "Javascript, HTML, CSS",
        platforms: "Web-based / browser",
        category: "University, Website",
        source: PROFILE_URL + "space-sequence",
        image: SpaceSequenceImage,
    },
    {
        title: "Binary Emulator",
        description: "Binary emulator for (limited) LEGv8/ARMv8",
        languages: "C",
        platforms: "PC",
        category: "University, C program",
        source: PROFILE_URL + "BinaryEmulator",
    },
    {
        title: "Interval Treap",
        description: "Interval treap (Binary (T)ree + H(eap)) implementation",
        languages: "Java",
        platforms: "PC",
        category: "University",
        source: PROFILE_URL + "COMS311-Project1",
    },
    {
        title: "astrelion.gitlab.io",
        description: "This website!",
        link: "https://astrelion.com",
        languages: "Javascript (React), HTML, CSS",
        platforms: "Web-based / browser",
        category: "Website",
        source: PROFILE_URL + "astrelion.gitlab.io",
        image: AstrelionImage,
    }
];

export default function ProjectsPage()
{
    return <Layout>
        <Project>
            This is just an excerpt; all my open-source projects can be found {}
            <a 
                href="https://gitlab.com/ASTRELION"
                title="I recently moved from GitHub, so some things aren't in true chronological order." 
                target="_blank" rel="noreferrer"
            >
                here
            </a>.
        </Project>
        {projects.map(p => createComponent(p))}
        <i>In no particular order, roughly grouped by category</i>
    </Layout>
}

function createComponent(project)
{
    return <Project 
        key={project.title}
        title={project.title}
        description={project.description}
        link={project.link}
        languages={project.languages}
        platforms={project.platforms}
        category={project.category}
        source={project.source}
        image={project.image}
    >
        {project.children}
    </Project>
}