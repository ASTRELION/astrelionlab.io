import React from 'react'
import PropTypes from 'prop-types'
import Link from 'gatsby-link'
import Helmet from 'react-helmet'

import Header from './header/header'

import favicon16 from '../../static/favicon-16x16.png'
import favicon32 from '../../static/favicon-32x32.png'
import favicon192 from '../../static/favicon-192x192.png'
import favicon512 from '../../static/favicon-512x512.png'
import faviconico from '../../static/favicon.ico'

import KingIcon from '../../static/KING_Icon-Plain-black.png'

import * as layoutStyles from './layout.module.css'

export default function Layout(props)
{
    return <div>
        <Helmet
            title="ASTRELION"
            meta={[
                { name: 'description', content: "ASTRELION's project site" },
                { name: 'keywords', content: 'astrelion, projects, gitlab, pages' }
            ]}
            link={[
                { rel: "icon", href: `${faviconico}` },
                { rel: "icon", type: "image/png", sizes: "16x16", href: `${favicon16}` },
                { rel: "icon", type: "image/png", sizes: "32x32", href: `${favicon32}` },
                { rel: "icon", type: "image/png", sizes: "192x192", href: `${favicon192}` },
                { rel: "icon", type: "image/png", sizes: "512x512", href: `${favicon512}` },
            ]}
        />
        <Header title={props.title} />
        <div
            style={{
                margin: '0 auto',
                maxWidth: 960,
                padding: '0px 1.0875rem 1.45rem',
                paddingTop: 0,
            }}
        >
            <div className={layoutStyles.background} />
            {props.children}
        </div>
    </div>  
}