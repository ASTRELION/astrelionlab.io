import React from 'react'
import Link from 'gatsby-link'

import * as MoreStyles from './more.module.css'

export default function More(props)
{
    return <div>
        <div className={`${MoreStyles.hide} ${MoreStyles.toggleable}`}>
            {props.children}
        </div>
        <button className="btn btn-outline-info btn-sm" onClick={e => toggle(e.target)}>More</button>
    </div>
}

function toggle(button)
{
    let elements = document.getElementsByClassName(MoreStyles.toggleable);
    
    for (let i = 0; i < elements.length; i++)
    {
        let element = elements[i];

        if (element.classList.contains(MoreStyles.hide))
        {
            button.innerHTML = "Less";
            element.classList.remove(MoreStyles.hide);
            element.classList.add(MoreStyles.show);
        }
        else
        {
            button.innerHTML = "More";
            element.classList.add(MoreStyles.hide);
            element.classList.remove(MoreStyles.show);
        }
    }
}