import React from 'react'
import Link from 'gatsby-link'

import { globalHistory } from "@reach/router"

import * as headerStyles from "./header.module.css"

import KingIcon from "../../../static/KING_Icon-Plain-black.png"



function ListLink(props)
{
    return <li className={headerStyles.headerButton} style={{ display: `inline-block`, margin: `0` }}>
      <Link to={props.to}>{props.children}</Link>
    </li>
}

export default function Header(props)
{
    const path = globalHistory.location.pathname;

    return <div
        className={headerStyles.header}
        style={{
            background: 'cyan',
            marginBottom: '1.45rem',
        }}
    >
        <div
            style={{
                margin: '0 auto',
                maxWidth: 960,
                padding: '0.5rem 1.0875rem',
                display: `flex`,
                alignItems: `center`
            }}
        >
            <Link
                to="/"
                style={{
                    color: 'hsla(0, 0%, 0%, 0.8)',
                    textDecoration: 'none',
                    width: `50%`
                }}
            >
                <h1 style={{ margin: 0 }}>
                    <div className={headerStyles.headerTitle}>
                        [root@astrelion.com {path}]$
                    </div>
                </h1>
            </Link>
            <div style={{ width: `50%` }}>
                <ul style={{ listStyle: `none`, float: `right`, margin: `0`, padding: `0` }}>
                    <ListLink to="">.</ListLink>
                    <ListLink to="/">..</ListLink>
                    <ListLink to="/projects/">projects/</ListLink>
                    <ListLink to="/about/">about/</ListLink>
                </ul>
            </div>
        </div>
    </div>
}