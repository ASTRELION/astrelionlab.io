import React from 'react'
import Link from 'gatsby-link'

import * as projectStyles from './project.module.css'

export default function Project(props)
{
    return <div className={`${projectStyles.project} row`}>
        <div className="col-sm-9">
            <h2 className={projectStyles.title}>
                {!props.link && props.source == "private" ?
                    props.title
                :
                    <a href={props.link ?? props.source} target="_blank" rel="noreferrer" title={props.link ?? props.source}>
                        {props.title}
                    </a>
                }
            </h2>
            <div className={projectStyles.info}>
                <div>{props.description}</div>
                <div>
                    {!props.link && props.source == "private" ?
                        <i>private</i>
                    : 
                        <a href={props.link ?? props.source} target="_blank" rel="noreferrer">
                            {props.link ?? props.source}
                        </a>
                    }
                </div>
            </div>
            <div>
                <div className={projectStyles.logistics}>
                    {props.languages ? 
                        <div className="row">
                            <div className="col-sm-3">
                                <span title="Programming languages used">Language(s):</span> 
                            </div>
                            <div className="col-sm-9">
                                {props.languages}
                            </div>
                        </div>
                    : null}
                    {props.platforms ?
                        <div className="row">
                            <div className="col-sm-3">
                                <span title="Development platforms used and/or distribution target(s)">Platform(s):</span>
                            </div>
                            <div className="col-sm-9">
                                {props.platforms}
                            </div>
                        </div>
                    : null}
                    {props.category ?
                        <div className="row">
                            <div className="col-sm-3">
                                <span title="The type of project">Category:</span>
                            </div>
                            <div className="col-sm-9">
                                {props.category}
                            </div>
                        </div>
                    : null}
                    {props.source ?
                        <div className="row">
                            <div className="col-sm-3">
                                <span title="Link to the source git repository">Source:</span>
                            </div>
                            <div className="col-sm-9">
                                {props.source != "private" ?
                                    <a href={props.source} target="_blank" rel="noreferrer">
                                        {props.source}
                                    </a>
                                :
                                    props.source
                                }
                            </div>
                        </div>
                    : null}
                </div>
                {props.children}
            </div>
        </div>
        {props.image ?
        <div className={`col-sm-3`}>
            <div className={projectStyles.imageContainer}>
                <a href={props.link ?? props.source}>
                    <img className={projectStyles.image} src={props.image} alt={`${props.title} image`}/>
                </a>
            </div>
        </div>
        :
        null}
    </div>
}